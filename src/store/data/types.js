export const SET_DROPDOWN = 'data/SET_DROPDOWN'
export const SET_SELECTED = 'data/SET_SELECTED'
export const SET_ASSETS = 'data/SET_ASSETS'
export const SET_ITEMS = 'data/SET_ITEMS'