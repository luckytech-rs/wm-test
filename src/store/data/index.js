import * as types from './types'

const state = {
    dropdown: null,
    selected: null,
    assets: null,
    items: null
}

const getters = {
    dropdown: state => state.dropdown,
    selected: state => state.selected,
    assets: state => state.assets,
    items: state => state.items,
}

const actions = {
    setDropdown ({commit}, data) {
        commit(types.SET_DROPDOWN, {dropdown: data})
    },
    setSelected ({commit}, data) {
        commit(types.SET_SELECTED, {selected: data})
    },
    setAssets ({commit}, data) {
        commit(types.SET_ASSETS, {assets: data})
    },
    setItems ({commit}, data) {
        commit(types.SET_ITEMS, {items: data})
    },
}

const mutations = {
    [types.SET_DROPDOWN] (state, {dropdown}) {
        state.dropdown = dropdown
    },
    [types.SET_SELECTED] (state, {selected}) {
        state.selected = selected
    },
    [types.SET_ASSETS] (state, {assets}) {
        state.assets = assets
    },
    [types.SET_ITEMS] (state, {items}) {
        state.items = items
    }
}

export default {
    state,
    getters,
    actions,
    mutations
  }
