import axios from 'axios'
import config from '@/config'
// import bus from '@/services/bus'


axios.defaults.baseURL = config.api

axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8,text/html; charset=utf-8'
axios.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded'

axios.defaults.headers.get['Accept-Language'] = 'en'

export function requestTransformer (request) {
  // bus.$emit("loader/start");
  return request
}


export function responseTransformer (response) {
  // bus.$emit("loader/stop");

  return response ? response.data : null
}

export function errorTransformer (error) {
  const errorMessage = error.response ? error.response.data : error
  // bus.$emit("loader/stop");
  throw errorMessage
}

export function extractData (response) {
  return response && response.data
}

export function paramsTransformer (params) {
  let param = "?"
  if (params) {
      for (let i = 0; i < params.length; i++) {
          let keyName = Object.keys(params[i])
          let keyVal = params[i][keyName]
          if (params.length - i > 1) {
              param += `${keyName}=${keyVal}&`
          } else {
              param += `${keyName}=${keyVal}`
          }

      }
  } else {
      param = ''
  }
  return param
}
